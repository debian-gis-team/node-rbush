node-rbush (2.0.2-4) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.4.1, no changes.
  * Update gbp.conf to use --source-only-changes by default.

 -- Bas Couwenberg <sebastic@debian.org>  Tue, 28 Aug 2018 14:19:53 +0200

node-rbush (2.0.2-3) unstable; urgency=medium

  * Bump Standards-Version to 4.2.0, no changes.
  * Update watch file to use releases instead of tags.
  * Update watch file to limit matches to archive path.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 18 Aug 2018 19:39:07 +0200

node-rbush (2.0.2-2) unstable; urgency=medium

  * Update copyright-format URL to use HTTPS.
  * Update Vcs-* URLs for Salsa.
  * Bump Standards-Version to 4.1.5, no changes.
  * Strip trailing whitespace from control & rules files.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 20 Jul 2018 21:56:28 +0200

node-rbush (2.0.2-1) unstable; urgency=medium

  * New upstream release.
  * Change Section from web to javascript.
  * Bump Standards-Version to 4.1.2, no changes.
  * Require at least node-quickselect 1.0.1.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 24 Dec 2017 12:42:23 +0100

node-rbush (2.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump priority to optional.
  * Add node-quickselect to dependencies.
  * Update install file for rbush.js to index.js rename.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 02 Jul 2016 23:56:27 +0200

node-rbush (1.4.3-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-Git URL to use HTTPS.
  * Update watch file to handle common issues.
  * Update copyright years for Vladimir Agafonkin.
  * Bump Standards-Version to 3.9.8, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 18 May 2016 07:35:46 +0200

node-rbush (1.4.2-1) unstable; urgency=medium

  * New upstream release.
  * Enable parallel builds.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 17 Dec 2015 04:11:46 +0100

node-rbush (1.4.1-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years for Vladimir Agafonkin.
  * Update Vcs-Browser URL to use HTTPS.
  * Drop XS-Testsuite header in control file.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 20 Sep 2015 11:17:43 +0200

node-rbush (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fix copyright year in copyright file.
  * Install rbush.js in /usr/lib/nodejs/rbush despite it being a single file.
  * Add upstream metadata.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 25 Apr 2015 11:48:26 +0200

node-rbush (1.3.5-1) unstable; urgency=medium

  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 27 Feb 2015 09:03:35 +0100

node-rbush (1.3.4-2) unstable; urgency=medium

  * Change Maintainer from Debian Javascript Maintainers to Debian GIS
    Project.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 26 Feb 2015 17:03:01 +0100

node-rbush (1.3.4-1) unstable; urgency=low

  [ Johan Van de Wauw ]
  * Initial release (Closes: #774568)
  * Initial debian files (from npm2deb).
  * Correct license in copyright file.

  [ Bas Couwenberg ]
  * Add myself to Uploaders.
  * Bump debhelper compatibility to 9.
  * Restructure control file with cme.
  * Use pkg-grass for Vcs-* URLs, use cgit for Vcs-Browser URL.
  * Add gbp.conf to use pristine-tar by default.
  * Use capitals in Upstream-Name & fix Copyright format in copyright file.
  * Install rbush.js in /usr/lib/nodejs because it's a single file.
  * Include visualization examples.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Feb 2015 16:35:35 +0100
